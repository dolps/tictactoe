# README #

See build.gradle for plugin version.
run gradle build SDK must be pointed to i think AS does this for you.

If running from a IDE like AS,
install the lombok plugin as i've used that on my objects for easy and fast refactoring.
I hope there will be no problem with that.

cmd + shift + a
plugins
install lombok intellij plugin

### What is this repository for? ###
This game of Tic Tac Toe is for the Mobile app development course at Westerdals.
MVP Guidelines are followed throughout the application.
And the modules are divided into a set of responsibilities

common -> Base classes
data -> Database handling classes
game -> MVP for game
gameMenu -> MVP for the menu
scoreHistory -> MVP for the Scorehistory
util -> util for adding/ swapping fragmentsaa

MainActivity works as a controller for the fragments
OnSwapView callback to init work to be done by the MainActivity

res -> All view + value files


# Application Flow
1. open app
2. add p1 and p2 names
3. click next button (fields will be verified)
4. if fields ok, you get taken to game
5. play a game of tic tac toe
6. on game end winner or status is showed
7. results are saved to DB and added to the current game score
8. on click play again game will be refreshed
9. click history you will be navigated to score history
10. score history is displayed in descending order


# Strength/  weaknesses of solution
Strengths are, its a solid application logic is even unit tested, MVP guidelines are followed for
great seperation of consern. Its Easy to mainain and add feature. There is a seperate data layer, which can be mocked out
and injected into the presenters. Only the view has dependency of Android specific stuff. The design is simple but at the same time intuitive and clean.
Coming in to this project without former knowledge should be easy as the packages should be
self explanatory.

# Weaknesses
No tests on datalayer/ viewlayer and simple design.
More testing should have been prioritized, but since this is an MVP application,
only testing of the view would have been android specific, and functional testing on all
view- functionality has been done.
