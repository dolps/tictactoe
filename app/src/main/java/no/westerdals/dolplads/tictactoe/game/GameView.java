package no.westerdals.dolplads.tictactoe.game;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import no.westerdals.dolplads.tictactoe.R;
import no.westerdals.dolplads.tictactoe.common.BaseFragment;
import no.westerdals.dolplads.tictactoe.model.Player;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Main Game window
 */

public class GameView extends BaseFragment implements GameContract.View {
    private static final String TAG = GameView.class.getName();
    private GameContract.Presenter presenter;

    @BindView(R.id.gameBoard)
    protected GridLayout gameBoard;

    @BindView(R.id.p1piece)
    protected TextView p1Piece;
    @BindView(R.id.p1name)
    protected TextView p1Name;
    @BindView(R.id.p1score)
    protected TextView p1Score;
    @BindView(R.id.p2piece)
    protected TextView p2Piece;
    @BindView(R.id.p2name)
    protected TextView p2Name;
    @BindView(R.id.p2score)
    protected TextView p2Score;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.game_view, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void setPresenter(GameContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public static GameView newInstance() {
        return new GameView();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @OnClick({R.id.btn00, R.id.btn01, R.id.btn02, R.id.btn10, R.id.btn11, R.id.btn12, R.id.btn20, R.id.btn21, R.id.btn22})
    public void onClick(View view) {
        Button button = (Button) view;

        String tag = button.getTag().toString();
        int row = Integer.valueOf(tag.substring(0, 1));
        int col = Integer.valueOf(tag.substring(1, 2));

        presenter.onButtonSelected(row, col);
    }

    @Override
    public void showTilePlayed(int row, int col, String playerInitials) {
        Button btn = (Button) gameBoard.findViewWithTag("" + row + col);
        btn.setText(playerInitials);
    }

    @Override
    public void announceWinner(String text) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View prompt = inflater.inflate(R.layout.announce_winner_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false)
                .setMessage(text)
                .setView(prompt)
                .setPositiveButton("Play again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.playAgain();
                    }
                });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void clearBoard() {
        for (int i = 0; i < gameBoard.getChildCount(); i++) {
            Button button = (Button) gameBoard.getChildAt(i);
            button.setText("");
        }
    }

    @Override
    public void showUsers(Player p1, Player p2) {
        p1Piece.setText(Player.nameAsInitial(p1.getName()));
        String p1N = p1.getName() + ":";
        p1Name.setText(p1N);
        p1Score.setText(String.valueOf(p1.getInGameScore()));

        p2Piece.setText(Player.nameAsInitial(p2.getName()));
        String p2N = p2.getName() + ":";
        p2Name.setText(p2N);
        p2Score.setText(String.valueOf(p2.getInGameScore()));
    }
}
