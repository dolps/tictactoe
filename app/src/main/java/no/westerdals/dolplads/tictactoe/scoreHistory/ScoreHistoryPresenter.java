package no.westerdals.dolplads.tictactoe.scoreHistory;

import android.util.Log;

import java.util.List;

import no.westerdals.dolplads.tictactoe.data.ScoreRepository;
import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Presents the score history for the view
 */

public class ScoreHistoryPresenter implements ScoreHistoryContract.Presenter {
    private static final String TAG = ScoreHistoryPresenter.class.getName();
    private final ScoreHistoryContract.View historyView;
    private final ScoreRepository scoreRepository;

    public ScoreHistoryPresenter(ScoreRepository scoreRepository, ScoreHistoryContract.View historyView) {
        this.historyView = historyView;
        this.scoreRepository = scoreRepository;
    }

    @Override
    public void start() {
        List<Score> scores = scoreRepository.findAllOrderByValue();
        historyView.showScores(scores);
    }
}
