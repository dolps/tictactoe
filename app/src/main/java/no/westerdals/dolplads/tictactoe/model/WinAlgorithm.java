package no.westerdals.dolplads.tictactoe.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Algorithm for checking if a player won the game, dont thank me im just following  prinsiples for consultants
 * Check this case study for more information and what seems to be a cool project
 * https://www3.ntu.edu.sg/home/ehchua/programming/java/javagame_tictactoe_ai.html
 */
@Data
public class WinAlgorithm {
    private final int ROWS;
    private final int COLS;

    private final int[] winningPatterns = {
            0b111000000, 0b000111000, 0b000000111, // rows
            0b100100100, 0b010010010, 0b001001001, // cols
            0b100010001, 0b001010100               // diagonals
    };

    public WinAlgorithm(int rows, int cols) {
        ROWS = rows;
        COLS = cols;
    }

    public boolean hasWon(final Player thePlayer, final Tile[][] tiles) {
        int pattern = 0b000000000;  // 9-bit pattern for the 9 cells
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                if (tiles[row][col].getPlayer() == thePlayer) {
                    pattern |= (1 << (row * 3 + col));
                }
            }
        }
        for (int winningPattern : winningPatterns) {
            if ((pattern & winningPattern) == winningPattern) return true;
        }
        return false;
    }
}
