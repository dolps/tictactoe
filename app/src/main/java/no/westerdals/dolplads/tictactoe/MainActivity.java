package no.westerdals.dolplads.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import no.westerdals.dolplads.tictactoe.game.GamePresenter;
import no.westerdals.dolplads.tictactoe.game.GameView;
import no.westerdals.dolplads.tictactoe.model.TicTacToeBoard;
import no.westerdals.dolplads.tictactoe.gameMenu.GameMenuPresenter;
import no.westerdals.dolplads.tictactoe.gameMenu.GameMenuView;
import no.westerdals.dolplads.tictactoe.model.Player;
import no.westerdals.dolplads.tictactoe.model.WinAlgorithm;
import no.westerdals.dolplads.tictactoe.scoreHistory.ScoreHistoryPresenter;
import no.westerdals.dolplads.tictactoe.data.ScoreRepository;
import no.westerdals.dolplads.tictactoe.scoreHistory.ScoreHistoryView;
import no.westerdals.dolplads.tictactoe.util.ActivityUtil;

/**
 * Created by dolplads on 28/05/2017.
 * Controller for the fragments MainActivity is only activity as only
 * flow needed for the game
 */
public class MainActivity extends AppCompatActivity implements OnSwapView {
    private static final String TAG = MainActivity.class.getName();
    private final int ROWS = 3;
    private final int COLS = 3;
    private ScoreRepository scoreRepository;
    @BindView(R.id.btnToHistory)
    protected Button btnScoreHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_wrapper);
        ButterKnife.bind(this);
        final GameMenuView gameMenuView = GameMenuView.newInstance();
        GameMenuPresenter gameMenuPresenter = new GameMenuPresenter(gameMenuView);
        gameMenuView.setPresenter(gameMenuPresenter);

        ActivityUtil.addFragment(getSupportFragmentManager(), gameMenuView, R.id.fragment_container);

        scoreRepository = new ScoreRepository(MainActivity.this);
    }


    @Override
    public void toGame(String p1, String p2) {
        btnScoreHistory.setVisibility(View.VISIBLE);
        final GameView gameView = GameView.newInstance();
        GamePresenter gamePresenter = new GamePresenter(new Player(p1), new Player(p2), new TicTacToeBoard(new WinAlgorithm(ROWS, COLS)), scoreRepository, gameView);
        gameView.setPresenter(gamePresenter);

        ActivityUtil.replaceFragment(getSupportFragmentManager(), gameView, R.id.fragment_container);
    }

    @OnClick(R.id.btnToHistory)
    public void toHistory() {
        btnScoreHistory.setVisibility(View.GONE);
        final ScoreHistoryView historyView = ScoreHistoryView.newInstance();
        ScoreHistoryPresenter historyPresenter = new ScoreHistoryPresenter(scoreRepository, historyView);
        historyView.setPresenter(historyPresenter);

        ActivityUtil.replaceFragment(getSupportFragmentManager(), historyView, R.id.fragment_container);
    }
}