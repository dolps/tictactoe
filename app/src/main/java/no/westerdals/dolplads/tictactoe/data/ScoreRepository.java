package no.westerdals.dolplads.tictactoe.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Methods for handling updates needed for storage of the scores
 */

public class ScoreRepository implements IScoreRepository {
    private static final String TAG = ScoreRepository.class.getName();
    private DbHelper dbHelper;
    private String[] columns = {ScorePersistence.ScoreEntry.COLUMN_NAME, ScorePersistence.ScoreEntry.COLUMN_VALUE};

    public ScoreRepository(Context context) {
        this.dbHelper = new DbHelper(context);
    }

    // used
    @Override
    public void createOrUpdate(Score score) {
        Score persisted = findByPlayerName(score.getUserName());

        if (persisted != null) {    // update
            persisted.setValue(persisted.getValue() + score.getValue());
            boolean updated = update(persisted);
            if (!updated) {
                throw new RuntimeException("should have been updated");
            }
        } else {    // create
            boolean created = create(score);
            if (!created) {
                throw new RuntimeException("should have been created");
            }
        }
    }

    @Override
    public List<Score> findAllOrderByValue() {
        List<Score> scores = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(ScorePersistence.ScoreEntry.TABLE_NAME, columns, null, null, null, null, ScorePersistence.ScoreEntry.COLUMN_VALUE + " DESC");

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Score found = createFromCursor(cursor);
                scores.add(found);
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        db.close();

        return scores;
    }

    private boolean create(Score score) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ScorePersistence.ScoreEntry.COLUMN_NAME, score.getUserName());
        values.put(ScorePersistence.ScoreEntry.COLUMN_VALUE, score.getValue());

        long created = db.insert(ScorePersistence.ScoreEntry.TABLE_NAME, null, values);
        db.close();

        return created > 0;
    }

    private boolean update(Score score) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ScorePersistence.ScoreEntry.COLUMN_VALUE, score.getValue());
        String whereClause = ScorePersistence.ScoreEntry.COLUMN_NAME + " LIKE ?";
        String[] whereArgs = {score.getUserName()};

        int n = db.update(ScorePersistence.ScoreEntry.TABLE_NAME,
                values,
                whereClause,
                whereArgs
        );

        db.close();

        return true;
    }

    private Score findByPlayerName(String userName) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String whereClause = ScorePersistence.ScoreEntry.COLUMN_NAME + " LIKE ?";
        String[] whereArgs = {userName};

        Cursor cursor = db.query(ScorePersistence.ScoreEntry.TABLE_NAME,
                columns, whereClause, whereArgs, null, null, null);

        Score foundScore = null;

        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                foundScore = createFromCursor(cursor);
            }
        }

        if (cursor != null) {
            cursor.close();
        }
        db.close();

        return foundScore;
    }

    private Score createFromCursor(Cursor cursor) {
        String playerName = cursor.getString(cursor.getColumnIndexOrThrow(ScorePersistence.ScoreEntry.COLUMN_NAME));
        int value = cursor.getInt(cursor.getColumnIndexOrThrow(ScorePersistence.ScoreEntry.COLUMN_VALUE));

        Score score = new Score(playerName, value);

        Log.d(TAG, "score cerated: " + score);

        return score;
    }
}
