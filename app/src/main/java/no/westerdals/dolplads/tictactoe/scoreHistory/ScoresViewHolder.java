package no.westerdals.dolplads.tictactoe.scoreHistory;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import no.westerdals.dolplads.tictactoe.R;
import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Viewholder.. binds data with the view for the score item
 */

public class ScoresViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = ScoresViewHolder.class.getName();
    @BindView(R.id.name)
    protected TextView txtName;
    @BindView(R.id.playerScore)
    protected TextView txtValue;

    public ScoresViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(Score score) {
        Log.d(TAG, score.toString());

        txtName.setText(score.getUserName());
        txtValue.setText(String.valueOf(score.getValue()));
    }
}
