package no.westerdals.dolplads.tictactoe.gameMenu;

import no.westerdals.dolplads.tictactoe.common.BasePresenter;
import no.westerdals.dolplads.tictactoe.common.BaseView;

/**
 * Created by dolplads on 28/05/2017.
 */

public interface GameMenuContract {
    interface View extends BaseView<Presenter> {
    }

    interface Presenter extends BasePresenter {
    }
}
