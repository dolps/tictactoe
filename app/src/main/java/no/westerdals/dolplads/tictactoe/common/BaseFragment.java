package no.westerdals.dolplads.tictactoe.common;

import android.content.Context;
import android.support.v4.app.Fragment;

import no.westerdals.dolplads.tictactoe.OnSwapView;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * If callback needed can use this
 */

public abstract class BaseFragment extends Fragment {
    protected OnSwapView parentCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnSwapView) {
            parentCallback = (OnSwapView) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement " + OnSwapView.class.getName());
        }
    }
}
