package no.westerdals.dolplads.tictactoe.gameMenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import no.westerdals.dolplads.tictactoe.OnSwapView;
import no.westerdals.dolplads.tictactoe.R;
import no.westerdals.dolplads.tictactoe.common.BaseFragment;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Displays the current game
 */

public class GameMenuView extends BaseFragment implements GameMenuContract.View {
    private GameMenuContract.Presenter presenter;
    @BindView(R.id.txtP1Name)
    protected EditText p1Name;
    @BindView(R.id.txtp2Name)
    protected EditText p2Name;

    public static GameMenuView newInstance() {
        return new GameMenuView();
    }

    @Override
    public void setPresenter(GameMenuContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.game_menu_view, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.btnPlayGame)
    public void playGame() {
        boolean validInput = validateInput();
        if (validInput) {
            parentCallback.toGame(editTextToString(p1Name), editTextToString(p2Name));
        } else {
            showSnackbarMessage(getString(R.string.nameRequirements));
        }
    }

    private boolean validateInput() {
        return hasValidLength(editTextToString(p1Name)) && hasValidLength(editTextToString(p2Name)) && !p1Name.equals(p2Name);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    private void showSnackbarMessage(String text) {
        Snackbar snackbar = Snackbar.make(getView(), text, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private boolean hasValidLength(String text) {
        return text.length() > 1 && text.length() < 7;
    }

    private String editTextToString(EditText editText) {
        return editText.getText().toString().trim();
    }
}
