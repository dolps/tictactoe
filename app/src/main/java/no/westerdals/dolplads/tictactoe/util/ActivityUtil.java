package no.westerdals.dolplads.tictactoe.util;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Static util for swapping and adding fragments
 */
public class ActivityUtil {
    public static void addFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId) {
        fragmentManager.beginTransaction()
                .add(frameId, fragment)
                .commit();
    }

    public static void replaceFragment(@NonNull FragmentManager fragmentManager, @NonNull Fragment fragment, int frameId) {
        fragmentManager
                .beginTransaction()
                .replace(frameId, fragment)
                .addToBackStack(null)
                .commit();
    }
}
