package no.westerdals.dolplads.tictactoe.common;

/**
 * Created by dolplads on 28/05/2017.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
