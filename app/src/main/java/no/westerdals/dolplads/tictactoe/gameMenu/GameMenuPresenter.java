package no.westerdals.dolplads.tictactoe.gameMenu;

import android.util.Log;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Redirects happen from few so not much going on here, if data bindings needed
 * for later should be placed here
 */

public class GameMenuPresenter implements GameMenuContract.Presenter {
    private static final String TAG = GameMenuPresenter.class.getName();
    private final GameMenuContract.View view;

    public GameMenuPresenter(GameMenuContract.View view) {
        this.view = view;
    }

    @Override
    public void start() {
        Log.d(TAG, "START");
    }
}
