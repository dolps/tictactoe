package no.westerdals.dolplads.tictactoe.model;

import android.graphics.Point;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by dolplads on 28/05/2017.
 *
 * Representation of score
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Score {
    private String userName;
    private int value;
}
