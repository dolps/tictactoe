package no.westerdals.dolplads.tictactoe;

import android.widget.EditText;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Callback interface its intended use is for the fragments to notify MainActivity of
 * changes in the fragment stack making swaps in one place
 */

public interface OnSwapView {
    void toGame(String p1Name, String p2Name);
}
