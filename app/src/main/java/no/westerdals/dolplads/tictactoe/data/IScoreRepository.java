package no.westerdals.dolplads.tictactoe.data;

import java.util.List;

import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 */

public interface IScoreRepository {
    void createOrUpdate(Score score);

    List<Score> findAllOrderByValue();
}
