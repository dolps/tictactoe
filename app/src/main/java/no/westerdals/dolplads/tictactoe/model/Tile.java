package no.westerdals.dolplads.tictactoe.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Representation of a tile which contains the player
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Tile {
    private Player player;
}
