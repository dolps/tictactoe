package no.westerdals.dolplads.tictactoe.game;

import no.westerdals.dolplads.tictactoe.common.BasePresenter;
import no.westerdals.dolplads.tictactoe.common.BaseView;
import no.westerdals.dolplads.tictactoe.model.Player;

/**
 * Created by dolplads on 28/05/2017.
 */

public interface GameContract {
    interface View extends BaseView<Presenter> {
        void showTilePlayed(int row, int col, String tile);

        void announceWinner(String playerName);

        void clearBoard();

        void showUsers(Player p1, Player p2);
    }

    interface Presenter extends BasePresenter {
        void onButtonSelected(int row, int col);

        void playAgain();
    }
}
