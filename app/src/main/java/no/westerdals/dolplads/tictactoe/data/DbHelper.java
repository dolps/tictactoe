package no.westerdals.dolplads.tictactoe.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Db helper.. sets up database and tables
 */

public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private final static String DB_NAME = "tictactor";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + ScorePersistence.ScoreEntry.TABLE_NAME
                + "(" + ScorePersistence.ScoreEntry.COLUMN_NAME + " TEXT PRIMARY KEY, "
                + ScorePersistence.ScoreEntry.COLUMN_VALUE + " INTEGER)");

        Log.d("DBHELPER", "CREATED DB");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ScorePersistence.ScoreEntry.TABLE_NAME);
        Log.d("DBHELPER", "NEWVERSION: " + newVersion);
        onCreate(db);
    }
}
