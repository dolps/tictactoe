package no.westerdals.dolplads.tictactoe.scoreHistory;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import no.westerdals.dolplads.tictactoe.R;
import no.westerdals.dolplads.tictactoe.common.BaseFragment;
import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Recycle view showing of the scorehistory
 */

public class ScoreHistoryView extends BaseFragment implements ScoreHistoryContract.View {
    private ScoreHistoryContract.Presenter presenter;
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;
    private ScoresAdapter scoresAdapter;

    public static ScoreHistoryView newInstance() {
        return new ScoreHistoryView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scoresAdapter = new ScoresAdapter(getActivity(), new ArrayList<Score>(0));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.recycler_list, container, false);
        ButterKnife.bind(this, view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(scoresAdapter);

        return view;
    }

    @Override
    public void setPresenter(ScoreHistoryContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
        getActivity().findViewById(R.id.btnToHistory).setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().findViewById(R.id.btnToHistory).setVisibility(View.VISIBLE);
    }

    @Override
    public void showScores(List<Score> scores) {
        scoresAdapter.replaceData(scores);
    }
}
