package no.westerdals.dolplads.tictactoe.model;

import lombok.Data;
import no.westerdals.dolplads.tictactoe.model.Player;
import no.westerdals.dolplads.tictactoe.model.Tile;
import no.westerdals.dolplads.tictactoe.model.WinAlgorithm;

/**
 * Created by dolplads on 28/05/2017.
 * Handles state on the board used in the game
 */
@Data
public class TicTacToeBoard {
    private Tile[][] tiles;
    private final int ROWS;
    private final int COLS;
    private WinAlgorithm winAlgorithm;
    private int tilesPlayed = 0;
    private Player winner;

    public TicTacToeBoard(WinAlgorithm winAlgorithm) {
        this.winAlgorithm = winAlgorithm;

        ROWS = winAlgorithm.getROWS();
        COLS = winAlgorithm.getCOLS();

        fillTiles();
    }

    public boolean playTile(int row, int col, Player player) {
        if (canBePlaced(row, col, player)) {
            tiles[row][col].setPlayer(player);
            tilesPlayed++;

            return true;
        }

        return false;
    }

    public boolean gameFinished(Player player) {
        if (winAlgorithm.hasWon(player, tiles)) {
            winner = player;
            return true;
        }

        return tilesPlayed == (ROWS * COLS);
    }

    public void reset() {
        winner = null;
        tilesPlayed = 0;
        fillTiles();
    }


    private void fillTiles() {
        this.tiles = new Tile[ROWS][COLS];
        for (int i = 0; i < ROWS; i++) {
            for (int y = 0; y < COLS; y++) {
                tiles[i][y] = new Tile();
            }
        }
    }

    private boolean canBePlaced(int row, int col, Player player) {
        return tileAvailable(row, col) && !gameFinished(player);
    }

    private boolean tileAvailable(int row, int col) {
        return tiles[row][col].getPlayer() == null;
    }
}
