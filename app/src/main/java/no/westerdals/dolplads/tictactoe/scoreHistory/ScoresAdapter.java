package no.westerdals.dolplads.tictactoe.scoreHistory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import lombok.Data;
import no.westerdals.dolplads.tictactoe.R;
import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 * Adapter for the recycle view
 */
@Data
public class ScoresAdapter extends RecyclerView.Adapter<ScoresViewHolder> {
    private static final String TAG = ScoresAdapter.class.getName();
    private List<Score> scores;
    private LayoutInflater layoutInflater;

    public ScoresAdapter(Context context, List<Score> scores) {
        layoutInflater = LayoutInflater.from(context);
        this.scores = scores;
    }

    @Override
    public ScoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ScoresViewHolder(layoutInflater.inflate(R.layout.scores_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ScoresViewHolder viewHolder, int position) {
        final Score score = scores.get(position);
        viewHolder.setData(score);
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    public void replaceData(List<Score> scores) {
        setScores(scores);
        notifyDataSetChanged();
    }
}
