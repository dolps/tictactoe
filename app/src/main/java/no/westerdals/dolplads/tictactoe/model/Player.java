package no.westerdals.dolplads.tictactoe.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by dolplads on 28/05/2017.
 * Simple representation of a player
 */

@Data
public class Player {
    private String name;
    private int inGameScore;

    public Player(String name) {
        this.name = name;
    }

    public static String nameAsInitial(String text) {
        return text.substring(0, 1).toUpperCase();
    }
}
