package no.westerdals.dolplads.tictactoe.scoreHistory;

import java.util.List;

import no.westerdals.dolplads.tictactoe.common.BasePresenter;
import no.westerdals.dolplads.tictactoe.common.BaseView;
import no.westerdals.dolplads.tictactoe.model.Score;

/**
 * Created by dolplads on 28/05/2017.
 */

public interface ScoreHistoryContract {
    interface View extends BaseView<Presenter> {
        void showScores(List<Score> scores);
    }

    interface Presenter extends BasePresenter {
    }
}
