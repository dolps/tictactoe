package no.westerdals.dolplads.tictactoe.data;

import android.provider.BaseColumns;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * DB table values
 */

public class ScorePersistence {
    public static abstract class ScoreEntry implements BaseColumns {
        public static final String TABLE_NAME = "scores";
        public static final String COLUMN_NAME = "playername";
        public static final String COLUMN_VALUE = "score";
    }
}
