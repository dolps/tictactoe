package no.westerdals.dolplads.tictactoe.game;

import android.util.Log;

import no.westerdals.dolplads.tictactoe.data.IScoreRepository;
import no.westerdals.dolplads.tictactoe.model.Player;
import no.westerdals.dolplads.tictactoe.model.Score;
import no.westerdals.dolplads.tictactoe.model.TicTacToeBoard;

/**
 * Created by dolplads on 28/05/2017.
 * <p>
 * Presenter deals with the model updates the view
 */

public class GamePresenter implements GameContract.Presenter {
    private static final String TAG = GamePresenter.class.getName();
    private GameContract.View view;
    private TicTacToeBoard board;
    private IScoreRepository scoreRepository;

    private Player p1;
    private Player p2;

    private Player current;

    public GamePresenter(Player p1, Player p2, TicTacToeBoard board, IScoreRepository scoreRepository, GameContract.View view) {
        this.view = view;
        this.scoreRepository = scoreRepository;

        this.board = board;
        this.p1 = p1;
        this.p2 = p2;

        current = p1;
    }

    @Override
    public void start() {
        view.showUsers(p1, p2);
        Log.d(TAG, "Start");
    }

    @Override
    public void onButtonSelected(int row, int col) {
        boolean played = board.playTile(row, col, current);
        if (!played) return;

        if (board.gameFinished(current)) {
            if (board.getWinner() != null) {
                view.announceWinner("The winner is: " + current.getName());
                current.setInGameScore(current.getInGameScore() + 1);
                scoreRepository.createOrUpdate(new Score(current.getName(), 1));
            } else {
                view.announceWinner("there was a tie");
            }

            view.clearBoard();
        } else {
            view.showTilePlayed(row, col, Player.nameAsInitial(current.getName()));
            current = current == p1 ? p2 : p1;
        }
    }

    @Override
    public void playAgain() {
        current = p1;
        this.board.reset();
        view.showUsers(p1, p2);
    }
}
