package no.westerdals.dolplads.tictactoe.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by dolplads on 28/05/2017.
 * Test that the win logic is indeed correct
 */
public class TicTacToeBoardTest {
    private TicTacToeBoard ticTacToeBoard;
    private Player p1;
    private Player p2;

    @Before
    public void setUp() throws Exception {
        ticTacToeBoard = new TicTacToeBoard(new WinAlgorithm(3, 3));
        p1 = new Player("a1");
        p2 = new Player("b1");

    }

    @Test
    public void testCanWin() {
        ticTacToeBoard.playTile(0, 0, p1);
        assertFalse(ticTacToeBoard.gameFinished(p1));
        ticTacToeBoard.playTile(0, 1, p1);
        assertFalse(ticTacToeBoard.gameFinished(p1));
        ticTacToeBoard.playTile(0, 2, p1);

        assertTrue(ticTacToeBoard.gameFinished(p1));
        assertEquals("p1 should be winner", p1, ticTacToeBoard.getWinner());
    }

    // XYY
    // YXX
    // XXY
    @Test
    public void testCanBeEqualAndHaveGameComplete() {
        ticTacToeBoard.playTile(0, 0, p1);
        ticTacToeBoard.playTile(0, 1, p2);
        ticTacToeBoard.playTile(0, 2, p2);

        ticTacToeBoard.playTile(1, 0, p2);
        ticTacToeBoard.playTile(1, 1, p1);
        ticTacToeBoard.playTile(1, 2, p1);

        ticTacToeBoard.playTile(2, 0, p1);
        ticTacToeBoard.playTile(2, 1, p1);
        ticTacToeBoard.playTile(2, 2, p2);

        assertTrue(ticTacToeBoard.gameFinished(p1));
        assertEquals("there should be no winner", null, ticTacToeBoard.getWinner());
    }

}